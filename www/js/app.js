$('#fullpage').fullpage({
  anchors: ["section1", "section2", "section3", "section4", "section5"],
  navigation: true,
  navigationPosition: 'right',
  resize: false
});

$('.site_loading').fadeOut();

$(window).resize(function() {
  return window_box.resize();
});

$('.window').click(function() {
  return window_box.hide();
});

$('[data-window]').click(function() {
  var boxclass, content, element;
  element = $('#' + $(this).data('window'));
  content = element.html();
  boxclass = element.data('class');
  return window_box.show(content, boxclass);
});

$('input.styled[type=file]').each(function() {
  var group;
  $(this).after('<div class="pseudo-file"><span>' + $(this).attr('placeholder') + '</span></div>');
  group = $(this).closest('.control-group');
  $(group.children('.btn_browse_file')).prependTo(group.children('.pseudo-file'));
  return $(this).appendTo(group.children('.pseudo-file'));
});

$(document).on('change', 'input.styled[type=file]', function() {
  var field_value;
  field_value = $(this).val().replace("C:\\fakepath\\", '');
  return $(this).closest('.control-group').children('.pseudo-file').children('span').text(field_value);
});

$('.window .box').on('click', 'a.btn_take_photo', function() {
  return alert('Hello! I am just a button, but soon I will do something awesome =)');
});

var slide_back;

$('.intro .next').click(function() {
  var slide;
  if ($('.slider li:animated').length) {
    return false;
  }
  slide = $(this).closest('.slide_text');
  slide_back();
  return slide.effect({
    effect: 'drop',
    complete: function() {
      return slide.appendTo('.intro').show();
    }
  });
});

slide_back = function() {
  return $('.slider li:eq(1)').fadeIn('slow', function() {
    return $('.slider li:first').hide().appendTo('.slider');
  });
};

$('.intro_arrow').click(function() {
  return $('.intro .next:first').click();
});

(function(w, $) {
  w.window_box = {};
  window_box.init = function() {
    return $('.box').click(function(e) {
      return e.stopPropagation();
    });
  };
  window_box.hide = function() {
    $('.window').fadeOut('fast', function() {
      return $('.window .box').attr('class', 'box');
    });
    return $.fn.fullpage.setAllowScrolling(true);
  };
  window_box.show = function(content, boxclass) {
    $.fn.fullpage.setAllowScrolling(false);
    $('.window .box').html(content);
    if (boxclass !== void 0 && boxclass) {
      $('.window .box').addClass(boxclass);
    }
    $('.window').fadeIn();
    return window_box.resize();
  };
  window_box.resize = function() {
    var border_width, content_height, maximum_height;
    if (!$('.window:visible').length) {
      return;
    }
    $('.window .box').css({
      height: 'auto'
    });
    content_height = $('.window .box').height();
    border_width = $('.window .box').outerHeight() - content_height;
    maximum_height = $(window).height() - border_width * 4;
    if (maximum_height - content_height < 0) {
      $('.window .box').addClass('overwin').css({
        height: maximum_height
      });
    }
    return $('.window .box').css({
      marginTop: -$('.window .box').outerHeight() / 2
    });
  };
  return window_box.init();
})(window, jQuery);
