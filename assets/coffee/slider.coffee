# Home slider
$('.intro .next').click ->
	if $('.slider li:animated').length
		return false

	slide = $(this).closest('.slide_text')
	slide_back()
	slide.effect
		effect: 'drop'
		complete: ->
			slide.appendTo('.intro').show()

slide_back = ->
	$('.slider li:eq(1)').fadeIn('slow', ->
		$('.slider li:first').hide().appendTo('.slider')
	)

$('.intro_arrow').click ->
	$('.intro .next:first').click()