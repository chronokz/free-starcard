# Navigation
$('#fullpage').fullpage
	anchors: [
		"section1"
		"section2"
		"section3"
		"section4"
		"section5"
	]
	navigation: true
	navigationPosition: 'right'
	resize: false

# Window loading
$('.site_loading').fadeOut()

# Window events
$(window).resize ->
	window_box.resize()

$('.window').click ->
	window_box.hide()

$('[data-window]').click ->
	element = $('#'+$(this).data('window'))
	content = element.html()
	boxclass = element.data('class')
	window_box.show content, boxclass

# Form pseudo inputs
$('input.styled[type=file]').each ->
	$(this).after('<div class="pseudo-file"><span>'+$(this).attr('placeholder')+'</span></div>')
	group = $(this).closest('.control-group')
	$(group.children('.btn_browse_file')).prependTo(group.children('.pseudo-file'))
	$(this).appendTo(group.children('.pseudo-file'))

$(document).on 'change', 'input.styled[type=file]', ->
	field_value = $(this).val().replace("C:\\fakepath\\", '')
	$(this).closest('.control-group').children('.pseudo-file').children('span').text(field_value)


# Button for take a photo
$('.window .box').on 'click', 'a.btn_take_photo', ->
	alert 'Hello! I am just a button, but soon I will do something awesome =)'