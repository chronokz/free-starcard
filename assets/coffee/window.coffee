((w, $) ->
	w.window_box = {}

	window_box.init = ->
		$('.box').click (e) -> 
			e.stopPropagation()

	window_box.hide = ->
		$('.window').fadeOut 'fast', ->
			$('.window .box').attr('class',  'box')
		$.fn.fullpage.setAllowScrolling true

	window_box.show = (content, boxclass) ->
		$.fn.fullpage.setAllowScrolling false
		$('.window .box').html content
		if (boxclass != undefined and boxclass)
			$('.window .box').addClass boxclass
		$('.window').fadeIn()
		window_box.resize()

	window_box.resize = ->
		if !$('.window:visible').length
			return
		$('.window .box').css
			height: 'auto'

		content_height = $('.window .box').height()
		border_width = $('.window .box').outerHeight() - content_height
		maximum_height = $(window).height() - border_width*4

		if (maximum_height - content_height < 0)
			$('.window .box').addClass('overwin').css
				height: maximum_height
		
		$('.window .box').css
			marginTop: -$('.window .box').outerHeight()/2

	window_box.init()

) window, jQuery